<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<div class="stm-sort-by-options clearfix">
    <span><?php esc_html_e('Sort by:', 'ht_vehicles_listing'); ?></span>

    <select name="sort_order">
        <?php //todo: make this as array & apply_filters ?>
        <?php //todo: mark current as selected ?>
        <option value="date_high" selected><?php esc_html_e('Date: newest first', 'ht_vehicles_listing'); ?></option>
        <option value="date_low"><?php esc_html_e('Date: oldest first', 'ht_vehicles_listing'); ?></option>
        <option value="price_low"><?php esc_html_e('Price: lower first', 'ht_vehicles_listing'); ?></option>
        <option value="price_high"><?php esc_html_e('Price: highest first', 'ht_vehicles_listing'); ?></option>
        <option value="mileage_low"><?php esc_html_e('Mileage: lowest first', 'ht_vehicles_listing'); ?></option>
        <option value="mileage_high"><?php esc_html_e('Mileage: highest first', 'ht_vehicles_listing'); ?></option>
    </select>

</div>